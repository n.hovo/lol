module.exports = {
  // testMatch: ['**/e2e/**/*.test.ts'],
  moduleNameMapper: {
    '\\.(css|scss|txt)$': 'identity-obj-proxy',
  },
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  modulePaths: ['<rootDir>'],
  testEnvironment: 'jsdom',
}


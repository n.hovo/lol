const { Builder, Key, By } = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');
require('chromedriver');

(async function example() {
  let options = new chrome.Options();
  options.addArguments('--headless');
  options.addArguments('--no-sandbox');
  options.addArguments('--disable-dev-shm-usage');
  options.addArguments('--disable-gpu');
 
  const driver =  await new Builder()
    .forBrowser('chrome')
    .setChromeOptions(options)
    .build();

    await driver.get('https://google.com');
    await driver.sleep(2000)
    await driver.quit()
})()
